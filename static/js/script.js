//Скролл

const anchors = document.querySelectorAll('a[href*="#"]')

for (let anchor of anchors) {
  anchor.addEventListener('click', function (e) {
    e.preventDefault()
    
    const blockID = anchor.getAttribute('href')
    
    document.querySelector('' + blockID).scrollIntoView({
      behavior: 'smooth',
      block: 'start'
    })
  })
}

//Размер видео на маленьких экранах
var width = $(window).width();
if((width < 992) && (width > 768)) {
    $('iframe').attr("width", "188px");
    $('iframe').attr("height", "107px");
  }

if((width < 768) && (width > 320)) {
$('iframe').attr("width", (width - 20)+"px");
  $('iframe').attr("height", (width - width/2)+"px");
}


function toggleMenu(){
  if($('.menu-wrapper').hasClass('closed'))
  {
    $('.menu-wrapper, .menu-footer').toggle('fast');
    $('.menu-wrapper').removeClass('closed').addClass('opened');
    $('.main-menu').css('height', '-webkit-fill-available');
    $('.main-content').css('padding-top', '50px');
  } else if($('.menu-wrapper').hasClass('opened')){
  $('.menu-wrapper, .menu-footer').toggle('fast');
    $('.menu-wrapper').removeClass('opened').addClass('closed');
    $('.main-menu').animate({height: '120px'}, 200);
    $('.main-content').css('padding-top', '0px');
  }
}

function toggleColorList(id){
  $(".first-row, .second-row").find("a").each(function(){
    $(this).css('background-color', '#' + $(this).attr('id'));
  });
  $(id + '> div').toggle('fast');
}

//Slider

var currentSlide = 1;
var translateWidth = 0;
var slideCount = $('#slidewrapper li').length;
$('.slider-count').text(currentSlide + '/' + slideCount);

function nextSlide(){

  if (currentSlide == slideCount || currentSlide <= 0 || currentSlide > slideCount) {
    $('#slidewrapper').css('transform', 'translate(0, 0)');
    currentSlide = 1;
} else {
    translateWidth = -$('#viewport').width() * (currentSlide);
    $('#slidewrapper').css({
        'transform': 'translate(' + translateWidth + 'px, 0)',
        '-webkit-transform': 'translate(' + translateWidth + 'px, 0)',
        '-ms-transform': 'translate(' + translateWidth + 'px, 0)',
    });
    currentSlide++;
}

$('.slider-count').text(currentSlide + '/' + slideCount);
}

function prevSlide() {
  if (currentSlide == 1 || currentSlide <= 0 || currentSlide > slideCount) {
      translateWidth = -$('#viewport').width() * (slideCount - 1);
      $('#slidewrapper').css({
          'transform': 'translate(' + translateWidth + 'px, 0)',
          '-webkit-transform': 'translate(' + translateWidth + 'px, 0)',
          '-ms-transform': 'translate(' + translateWidth + 'px, 0)',
      });
      currentSlide = slideCount;
  } else {
      translateWidth = -$('#viewport').width() * (currentSlide - 2);
      $('#slidewrapper').css({
          'transform': 'translate(' + translateWidth + 'px, 0)',
          '-webkit-transform': 'translate(' + translateWidth + 'px, 0)',
          '-ms-transform': 'translate(' + translateWidth + 'px, 0)',
      });
      currentSlide--;
  }

  $('.slider-count').text(currentSlide + '/' + slideCount);
}