import React, {Component} from 'react';
import axios from 'axios';
import '../../../static/css/login.css';



export default class Register extends Component {

    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: '',
            registrationSuccess: 'none'
        };

    }

    componentWillMount() {
        document.title = 'Регистрация';
    }


    handleChangeEmail = email =>{

        this.setState({email});
    };


    handleChangePassword = password =>{

        this.setState({password});
    };

    handleLoginClick = () =>{

        this.setState({registrationSuccess: 'none'});
    };

    // Send data on server and get response
    handleSubmit = () => {

        axios.post(`http://188.225.46.7:8000/api/accounts/register/`,
            {
                'email': this.state.email,
                'password': this.state.password
            }
        ).then(res => {
            if(res.data.email) {
                this.setState({registrationSuccess: 'success'});
            }
            else {
                this.setState({registrationSuccess: 'none'});
            }

        }).catch((error) => {
            this.setState({registrationSuccess: 'error'});
            });
    };

    //render result to user
    renderResult = () => {
        var result;
        if (this.state.registrationSuccess === 'success')
            result = <div className="success">
                <i className="far fa-check-circle"></i>
                <h4>Успешно!</h4>
                <p>Ваш аккаунт успешно зарегистрирован! Письмо для подтверждения регистрации
                    отправлено вам на email.</p>
            </div>;
        else if (this.state.registrationSuccess === 'error')
            result = <div className="unsuccess">
                <i className="far fa-times-circle"></i>
                <h4>Ошибка!</h4>
                <p>На данный email уже зарегистрирован аккаунт.</p>
            </div>;
        else result = '';
        return result;
    };

    render() {

        return (

            <React.Fragment>
                <section className="login-section">
                    <div className="logo">
                        <img src="./img/logo.png" alt="Logo"/>
                    </div>
                    <div className="login-section-inner-wrapper">
                        <div className="login-wrapper">
                            <h3>Регистрация аккаунта Jam-Day</h3>
                            <form action="#" name="register-form">
                                <p><input type="email" name="email" id="e-mail" placeholder="Ваш email"
                                          onChange={event => this.handleChangeEmail(event.target.value)}/></p>
                                <p><input type="email" name="password" id="password" placeholder="Ваш пароль"
                                          onChange={event => this.handleChangePassword(event.target.value)}/></p>

                                {this.renderResult()}

                                <a href="#" className="button red"
                                   onClick={this.handleSubmit}>Регистрация аккаунта</a>
                                <a href="/login" className="button blue"
                                   onClick={this.handleLoginClick}>Уже есть аккаунт</a>
                            </form>
                            <p>Мы не разглашаем Ваши данные третьим лицам.</p>
                            <div className="license">
                                <a href="#">Политика конфиденциальности</a>
                                <p>/</p>
                                <a href="#">Договор-оферта</a>
                            </div>
                        </div>
                    </div>
                </section>
            </React.Fragment>


        );
    }
}
