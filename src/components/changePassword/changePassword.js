import React, {Component} from 'react';
import axios from 'axios';
import '../../../static/css/login.css';



export default class ChangePassword extends Component {

    constructor(props) {
        super(props);
        this.state = {
            email: '',
            changeSuccess: 'none'
        };

    }

    componentWillMount() {
        document.title = 'Смена пароля';
    }


    handleChangeEmail = email =>{

        this.setState({email});
    };


    // Send data on server and get response
    handleSubmit = () => {
        axios.post(`http://188.225.46.7:8000/account/registration/`,     // fix it
            {
                'email': this.props.email,
            }
        ).then(res => {

            alert(res);
            //redirect to Login

        })
    };


    render() {

        return (

            <React.Fragment>
                <section className="login-section">
                    <div className="logo">
                        <img src="./img/logo.png" alt="Logo"/>
                    </div>
                    <div className="login-section-inner-wrapper">
                        <div className="login-wrapper">
                            <h3>Смена пароля от аккаунта Jam-Day</h3>
                            <p><input type="text" name="new-pass" id="new-pass" placeholder="Новый пароль"
                                      onChange={event => this.handleChangeEmail(event.target.value)}/></p>
                            <a href="#" className="button red" onClick={this.handleSubmit}>Изменить пароль</a>
                            <a href="/login" className="button blue">Авторизация</a>
                        </div>
                    </div>
                </section>
            </React.Fragment>


        );
    }
}
