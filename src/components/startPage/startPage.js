/*
*  Index page render
*
* */

import React, {Component} from 'react';
import {Link} from "react-router-dom";
import '../../../static/css/main.css';



export default class StartPage extends Component {

    constructor(props) {
        super(props);

    }

    componentWillMount() {
        document.title = 'Jam-Day';
    }

    render() {

        return (
            <React.Fragment>
                <header>
                    <div className="content-wrapper">
                        <div id="header" className="header">
                            <div className="header-upper-items">
                                <div className="logo">
                                    <img src="./img/logo.png" alt="Logo"/>
                                </div>
                                <nav className="main-nav">
                                    <ul className="menu">
                                        <li className="menu-item-1"><a href="#header">Презентация</a></li>
                                        <li className="menu-item-2"><a href="#feedback">Отзывы</a></li>
                                        <li className="menu-item-3"><a href="#price">Стоимость</a></li>
                                        <li className="menu-item-4"><a href="#help">Контакты</a></li>
                                    </ul>
                                </nav>
                                <a href="/login" className="button orange">Войти в панель</a>
                            </div>
                            <div className="header-content">
                                <div className="presentation">
                                    <h2>Посмотреть презентацию</h2>
                                    <div className="presentation-video">
                                        <iframe width="344" height="193" src="https://www.youtube.com/embed/4TAXeUN6gd4" frameBorder="0"
                                                allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                                                allowFullScreen></iframe>
                                    </div>
                                </div>
                                <div className="header-information">
                                    <h1>Автоматический сервис поздравления подписчиков с днем рождения</h1>
                                    <p className="header-p1"><i>Удивляйте подписчиков красивыми поздравлениями с днем рождения и
                                        получайте в ответ лайки и комментарии.</i></p>
                                    <p className="header-p2"><b>Легко</b> и эффективно, <b>без специальных знаний</b>,<br/> с любого
                                        устройства, <b>всего в несколько кликов</b> <br/>продвигайте свои сообщества в соц. сетях и
                                            получайте <br/><b>Десятки новый "теплых" слов и лайков в день.</b></p>
                                    <p className="header-p3">Jam-Day найдет подписчиков с днем рождения и поздравит их от им</p>
                                    <a href="./register.html" className="button red">Попробовать 7 дней бесплатно</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </header>
                <div className="content">
                    <div id="feedback" className="feedback">
                        <div className="content-wrapper">
                            <div className="feedback-title">
                                <img src="./img/feedback-title.png" alt="Отзывы"/>
                                    <div className="feedback-title-headers">
                                        <h2>Отзывы</h2>
                                        <h3>наших клиентов</h3>
                                    </div>
                            </div>
                            <div className="feedback-content">
                                <div className="feedback-leftside">
                                    <div className="vk-feedback-block">
                                        <div className="feedback-block-title">
                                            <div className="feedback-block-headers">
                                                <h4><span id="name-1">Ольга</span> <span id="surname-1">Козлова</span></h4>
                                                <span id="group-1">Ростовые цветы/торшеры</span>
                                            </div>
                                            <img src="./img/vk-icon.png" alt="VK"/>
                                        </div>
                                        <div className="feedback-block-content">
                                            <p>
                                                <i>Пользуемся сервисом поздравлений Jam Day. Нашим подписчикам очень нравится, отвечают
                                                    в комментариях, посещают группу. Поздравления всегда публикуются в заданное время.
                                                    Очень удобный сервис!</i>
                                            </p>
                                            <div className="feedback-block-footer">
                                                <a href="#" className="button orange">Посмотреть поздравления</a>
                                                <span id="data-1">04.05.2018</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="ok-feedback-block">
                                        <div className="feedback-block-title">
                                            <div className="feedback-block-headers">
                                                <h4><span id="name-2">Ольга</span><span id="surname-2">Козлова</span></h4>
                                                <span id="group-2">Ростовые цветы/торшеры</span>
                                            </div>
                                            <img src="./img/ok-icon.png" alt="VK"/>
                                        </div>
                                        <div className="feedback-block-content">
                                            <p>
                                                <i>Пользуемся сервисом поздравлений Jam Day. Нашим подписчикам очень нравится, отвечают
                                                    в комментариях, посещают группу. Поздравления всегда публикуются в заданное время.
                                                    Очень удобный сервис!</i>
                                            </p>
                                            <div className="feedback-block-footer">
                                                <a href="#" className="button blue">Посмотреть поздравления</a>
                                                <span id="data-2">04.05.2018</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="feedback-rightside">
                                    <div className="ok-feedback-block">
                                        <div className="feedback-block-title">
                                            <div className="feedback-block-headers">
                                                <h4><span id="name-3">Ольга</span><span id="surname-3">Козлова</span></h4>
                                                <span id="group-3">Ростовые цветы/торшеры</span>
                                            </div>
                                            <img src="./img/ok-icon.png" alt="OK"/>
                                        </div>
                                        <div className="feedback-block-content">
                                            <p>
                                                <i>Пользуемся сервисом поздравлений Jam Day. Нашим подписчикам очень нравится, отвечают
                                                    в комментариях, посещают группу. Поздравления всегда публикуются в заданное время.
                                                    Очень удобный сервис!</i>
                                            </p>
                                            <div className="feedback-block-footer">
                                                <a href="#" className="button blue">Посмотреть поздравления</a>
                                                <span id="data-3">04.05.2018</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="vk-feedback-block">
                                        <div className="feedback-block-title">
                                            <div className="feedback-block-headers">
                                                <h4><span id="name-4">Ольга</span><span id="surname-4">Козлова</span></h4>
                                                <span id="group-4">Ростовые цветы/торшеры</span>
                                            </div>
                                            <img src="./img/vk-icon.png" alt="VK"/>
                                        </div>
                                        <div className="feedback-block-content">
                                            <p>
                                                <i>Пользуемся сервисом поздравлений Jam Day. Нашим подписчикам очень нравится, отвечают
                                                    в комментариях, посещают группу. Поздравления всегда публикуются в заданное время.
                                                    Очень удобный сервис!</i>
                                            </p>
                                            <div className="feedback-block-footer">
                                                <a href="#" className="button orange">Посмотреть поздравления</a>
                                                <span id="data-4">04.05.2018</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="feedback-footer">
                                <a href="#" className="button orange bordered">Посмотреть все отзывы</a>
                            </div>
                        </div>
                    </div>
                    <div id="price" className="price">
                        <div className="content-wrapper">
                            <div className="price-title">
                                <div className="price-title-headers">
                                    <h2>Стоимость</h2>
                                    <h3>нашей услуги</h3>
                                </div>
                                <img src="./img/price-title.png" alt="Стоимость"/>
                            </div>
                            <div className="price-header">
                                <h2>Тарифные планы</h2>
                            </div>
                            <div className="price-content">
                                <div className="price-leftside">
                                    <div className="price-block">
                                        <h3>"Фиксированный"</h3>
                                        <h5>Поздравляйте когда надо</h5>
                                        <p className="pricing"><span>10</span> руб</p>
                                        <p>За одно сообщество</p>
                                        <a href="#" className="button blue">Попробовать бесплатно</a>
                                    </div>
                                    <div className="price-block">
                                        <h3>"Пол года"</h3>
                                        <h5>Ежедневное поздравление</h5>
                                        <p className="pricing"><span>1100</span> руб</p>
                                        <p>За одно сообщество</p>
                                        <a href="#" className="button orange">Подключить группу</a>
                                    </div>
                                </div>
                                <div className="price-rightside">
                                    <div className="price-block">
                                        <h3>"Месяц"</h3>
                                        <h5>Ежедневное поздравление</h5>
                                        <p className="pricing"><span>200</span> руб</p>
                                        <p>За одно сообщество</p>
                                        <a href="#" className="button orange">Подключить группу</a>
                                    </div>
                                    <div className="price-block">
                                        <h3>"Год"</h3>
                                        <h5>Ежедневное поздравление</h5>
                                        <p className="pricing"><span>1900</span> руб</p>
                                        <p>За одно сообщество</p>
                                        <a href="#" className="button orange">Подключить группу</a>
                                    </div>
                                </div>
                            </div>
                            <div className="price-footer">
                                <p>За вторую и последующую группу, скидка - 10%</p>
                            </div>
                        </div>
                    </div>
                    <div id="help" className="help">
                        <div className="content-wrapper">
                            <div className="help-title">
                                <h2>Нужна помощь?</h2>
                                <p><i>Попробуйте следующее:</i></p>
                            </div>
                            <div className="help-cards">
                                <a href="#" className="help-card">
                                    <div className="help-card-icon"><img src="./img/faq-icon.png" alt="faq icon"/></div>
                                    <div className="help-card-text">
                                        <h5>Почитать FAQ</h5>
                                        <p>
                                            Найти ответы на часто задаваемые вопросы
                                        </p>
                                    </div>
                                </a>
                                <a href="#" className="help-card">
                                    <div className="help-card-icon"><img src="./img/bot-icon.png" alt="bot icon"/></div>
                                    <div className="help-card-text">
                                        <h5>Обратиться к боту</h5>
                                        <p>
                                            Наш чат-бот ответит Вам в течении 5 секунд
                                        </p>
                                    </div>
                                </a>
                                <a href="#" className="help-card inactive" onClick="$('.consult').toggle('normal');">
                                    <div className="help-card-icon"><img src="./img/mail-icon.png" alt="mail icon"/></div>
                                    <div className="help-card-text">
                                        <h5>Связаться с нами</h5>
                                        <p>
                                            Расскажите о Вашей проблеме нашим сотрудникам
                                        </p>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div id="consult" className="consult">
                        <div className="content-wrapper">
                            <h2>Консультанты в соц. сетях</h2>
                            <div className="consult-buttons">
                                <a href="#" className="button blue"><i className="fab fa-vk"></i> ВКонтакте</a>
                                <a href="#" className="button orange"><i className="fab fa-odnoklassniki"></i> Одноклассники</a>
                            </div>
                        </div>
                    </div>
                    <div className="advise">
                        <div className="content-wrapper">
                            <p>Не откладывайте на завтра - попробуйте сервис уже сегодня <br/> абсолютно бесплатно, и через 7 дней Вы уже
                                не захотите от него отказываться!</p>
                            <a href="#" className="button red bordered">Хочу попробовать 7 дней бесплатно</a>
                        </div>
                    </div>
                </div>
                <footer>
                    <div className="content-wrapper">
                        <div className="footer-nav">
                            <p>© Jam-Day 2019. Все права защищены.</p>
                            <ul className="footer-menu">
                                <li><a href="#">Главная</a></li>
                                <li><a href="#">Политика конфиденциальности</a></li>
                                <li><a href="#">Поддержка</a></li>
                                <li><a href="#">FAQ - Часто задаваемые вопросы</a></li>
                                <li><a href="#">Структура сайта</a></li>
                            </ul>
                        </div>
                        <div className="footer-social">
                            <a href="#"><i className="fab fa-vk"></i></a>
                            <a href="#"><i className="fab fa-instagram"></i></a>
                            <a href="#"><i className="fab fa-twitter"></i></a>
                            <a href="#"><i className="fab fa-youtube"></i></a>
                            <a href="#"><i className="fab fa-facebook-f"></i></a>
                            <a href="#"><i className="fab fa-odnoklassniki"></i></a>
                        </div>
                    </div>
                </footer>
            </React.Fragment>
        );
    }
}







