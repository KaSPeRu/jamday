import React, {Component} from 'react';
import axios from 'axios';
import '../../../static/css/login.css';



export default class Recovery extends Component {

    constructor(props) {
        super(props);
        this.state = {
            email: '',
            recoverySuccess: 'none'
        };

    }

    componentWillMount() {
        document.title = 'Восстановление пароля';
    }


    handleChangeEmail = email =>{

        this.setState({email});
    };


    // Send data on server and get response
    handleSubmit = () => {

        axios.post(`http://188.225.46.7:8000/api/accounts/password_reset/`,
            {
                'email': this.props.email
            }
        ).then(res => {

            this.setState({email});

        })

    };

    //render result to user
    renderResult = () => {
        var result;
        if (this.state.recoverySuccess === 'success')
            result = <div className="success">
                        <i className="far fa-check-circle"></i>
                        <h4>Успешно!</h4>
                        <p>На ваш email отправлено письмо для восстановления доступа.</p>
                    </div>;
        else if (this.state.recoverySuccess === 'error')
            result = <div className="unsuccess">
                        <i className="far fa-times-circle"></i>
                        <h4>Ошибка!</h4>
                        <p>Аккаунт с данным email не найден.</p>
                    </div>;
        else result = '';
        return result;
    };

    render() {

        return (

            <React.Fragment>
                <section className="login-section">
                    <div className="logo">
                        <img src="./img/logo.png" alt="Logo"/>
                    </div>
                    <div className="login-section-inner-wrapper">
                        <div className="login-wrapper">
                            <h3>Восстановление аккаунта Jam-Day</h3>
                            <p><input type="email" name="recovery" id="e-mail-recovery" placeholder="Ваш email"
                                      onChange={event => this.handleChangeEmail(event.target.value)}/></p>
                            {this.renderResult()}
                            <a href="#" className="button red" onClick={this.handleSubmit}>Восстановить аккаунт</a>
                            <a href="/login" className="button blue">Вспомнили пароль?</a>
                        </div>
                    </div>
                </section>
            </React.Fragment>


        );
    }
}
