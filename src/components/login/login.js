import React, {Component} from 'react';
import {Link, Route, Switch} from "react-router-dom";
import axios from "axios";
import '../../../static/css/login.css';



export default class Login extends Component {

    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: '',
            loginSuccess: 'none',
            VK: 'LOGIN_WITH_VK',
            OK: 'LOGIN_WITH_OK'
        }

    }

    componentWillMount() {
        document.title = 'Вход';
    }

    handleChangeEmail = email =>{

        this.setState({email});
    };


    handleChangePassword = password =>{

        this.setState({password});
    };

    handleLoginWithSocial = social => {
        axios.post(`http://188.225.46.7:8000/api/accounts/login/`,
            {
                'token': window.sessionStorage.getItem(vk_token)
            }
        ).then(res => {

            if(res.is_set) {
                this.setState({loginSuccess: 'success'});
            }
            else {
                this.setState({loginSuccess: 'none'});
            }

        })
    };

    // Send data on server and get response
    handleSubmit = () => {
        axios.post(`http://188.225.46.7:8000/api/accounts/login/`,
            {
                'email': this.state.email,
                'password': this.state.password
            }
        ).then(res => {

            if(res.data.token) {
                window.sessionStorage.setItem('token',res.data.token);
                document.location.href="/";
            }
            else {
                this.setState({loginSuccess: 'none'});
            }

        })
            .catch((error) => {
                this.setState({loginSuccess: 'error'});
            })
    };

    //render result to user
    renderResult = () => {
        var result;
        if (this.state.loginSuccess === 'error')
            result = <div className="unsuccess">
                <i className="far fa-times-circle"></i>
                <h4>Ошибка!</h4>
                <p>Аккаунт с данным email не найден.</p>
            </div>;
        else if (this.state.loginSuccess === 'none')
            result = '';
        else result = '';
        return result;
    };


    render() {

        return (

            <React.Fragment>
                <section className="login-section">
                    <div className="logo">
                        <img src="./img/logo.png" alt="Logo"/>
                    </div>
                    <div className="login-section-inner-wrapper">
                        <div className="social-login-wrapper">
                            <p>Вы проходите процедуру авторизации в сервисе "Jam-Day". Для авторизации/регистрации могут
                                быть использованы аккаунты в социальных сетях</p>
                            <a href="#" className="button blue" onClick={event => this.handleLoginWithSocial(event.target)}>
                                <i className="fab fa-vk"></i> Авторизация через
                                Вконтакте</a>
                            <a href="#" className="button orange"><i className="fab fa-odnoklassniki"></i> Авторизация
                                через Одноклассники</a>
                            <p>Мы не разглашаем Ваши данные третьим лицам.</p>
                            <div className="license">
                                <a href="#">Политика конфиденциальности</a>
                                <p>/</p>
                                <a href="#">Договор-оферта</a>
                            </div>
                        </div>
                        <div className="login-wrapper">
                            <h3>Авторизация через Email / Пароль</h3>
                            <div><a href="/recovery">Забыли пароль?</a></div>
                            <form action="#" name="login-form">
                                <p><input type="email" name="email" id="e-mail" placeholder="Ваш email"
                                          onChange={event => this.handleChangeEmail(event.target.value)}/></p>
                                <p><input type="email" name="password" id="password" placeholder="Ваш пароль"
                                          onChange={event => this.handleChangePassword(event.target.value)}/></p>

                                {this.renderResult()}

                                <div className="login-form-buttons">
                                    <a href="#" className="button red"
                                       onClick={this.handleSubmit}>Войти</a>
                                    <a href="/register" className="button blue">Создать аккаунт</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </section>
            </React.Fragment>


        );
    }
}