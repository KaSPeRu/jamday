/*
*  This is the root of the application.
*  In it we will implement routes between pages.
* */


import React, {Component} from 'react';
import {Route, Switch} from "react-router-dom";
import Login from "./components/login/login"
import Register from "./components/register/register"
import StartPage from "./components/startPage/startPage"
import Recovery from "./components/recoveryPassword/recoveryPassword"
import ChangePassword from "./components/changePassword/changePassword";


class App extends Component {

    constructor(props) {
        super(props);

    }
    render() {

        return (

            <Switch>
                <Route exact path='/'     render={(props)=><StartPage {...props} />}/>
                <Route path='/login'      render={(props)=><Login     {...props} />}/>
                <Route path='/register'   render={(props)=><Register     {...props} />}/>
                <Route path='/recovery'   render={(props)=><Recovery     {...props} />}/>
                <Route path='/change'     render={(props)=><ChangePassword     {...props} />}/>
            </Switch>


        );
    }
}

export default App;
