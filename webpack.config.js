const path = require("path");
const CopyPlugin = require('copy-webpack-plugin');
const HtmlWebpackPlugin = require("html-webpack-plugin");

module.exports = {
    entry: "./src/index.js",
    output: {
        path: path.join(__dirname, "dist"),
        filename: "bundle.js",
        publicPath: "/"
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader"
                },
            },
            {
                test: /\.(jpe?g|png|gif|svg)$/i,
                loaders: [
                    'file-loader?hash=sha512&digest=hex&name=[hash].[ext]',
                    'image-webpack-loader?bypassOnDebug&optimizationLevel=7&interlaced=false'
                ]
            },
            {
                test: /\.css$/,
                use: ["style-loader", "css-loader"]
            }
        ]
    },
    devServer: {
        stats: 'errors-only',
        historyApiFallback: true
    },
    plugins: [
        new CopyPlugin([
            { from: 'static/css', to: 'css' },
            { from: 'static/js', to: 'js' },
            { from: 'static/libs', to: 'libs' },
            { from: 'static/img', to: 'img' }
        ]),
        new HtmlWebpackPlugin({
            template: "./static/index.html"
        })
    ],

};